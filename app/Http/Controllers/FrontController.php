<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Sastrawi\Stemmer;
use Sastrawi\Morphology\Disambiguator\DisambiguatorInterface;
class FrontController extends Controller
{
    function spin(){
        if ($_POST['type_spin'] == 'spin') {
            
            $some = new Stemmer\StemmerFactory();
            $stemmer = $some->createStemmer();
            $results = $stemmer->stem($_POST['value_spin']);
           
            $data = ['data' =>  $results,'orig_word' => $_POST['value_spin'],'exclude_word' => $_POST['exclude_spintext']];
            $ch = curl_init(env('URL_API').'/spintext');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
    
    
            if ($err) {
                Log::error($err);
            }
            $final_word = "";
            if(is_array($result)){
                $final_word = implode(" ",$result);
            }else{
                $final_word = $result;
            }
            return redirect('/')->with(['result' => $final_word , 'orig_word' => $_POST['value_spin'],'exclude' => $_POST['exclude_spintext']]);
        }else{
            $data = [
                'articles'      =>  $_POST['value_spin'],
                'result_spin'   =>  $_POST['result_spin']
            ];
            $curl = curl_init(env('URL_API').'/spin_text');
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, ($data));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if($err){
                Log::error(print_r($err));
            }

           
            return redirect('/')->with([
                'orig_word' =>  $_POST['value_spin'],
                'result'    =>  $_POST['result_spin'],
                'final_articles'    =>  $result,
                'exclude'   => $_POST['exclude_spintext']
            ]);
        }
    }
}
