<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Front Spintags</title>
    <!-- style resource -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <!-- javascript resource -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
    <script src="{{asset('/js/app.js')}}"></script>
</head>
<body>
<div class="ui inverted segment content gradient center aligned ">
   <h2 class='ui horizontal divider inverted'>Spintext Article</h2>
   <p>
       Buat banyak artikel hanya dari 1 artikel tanpa terdeteksi sebagai spam !
   </p>
   <p>
       <div class="massive ui gray circular icon button">
            <i class="newspaper icon"></i>
       </div>
   </p>
</div>
<div class="ui container">
    <div class="ui basic center aligned segment">
        <div class="ui steps">
            <div class="step">
                <i class="paste icon"></i>
                <div class="content">
                <div class="title">Langkah 1</div>
                <div class="description">Paste artikel , beri kalimat pengecualian , tekan tombol Spintext</div>
                </div>
            </div>
            <div class="active step">
                <i class="pen square icon"></i>
                <div class="content">
                <div class="title">Langkah 2</div>
                <div class="description">Edit artikel hasil spintext, tekan tombol Spin</div>
                </div>
            </div>
            <div class="step">
                <i class="undo icon"></i>
                <div class="content">
                <div class="title">Langkah 3</div>
                <div class="description">klik tombol spin lagi untuk hasil spin yang lain</div>
                </div>
            </div>
        </div>
    </div>
    <form class="ui form" id="primary_form" action='/spin' method='POST'>
        @csrf
        <input type="hidden" name="type_spin" value="spin" id="type_spin">
            <div class="field">
                <label>Article to spin</label>
                <textarea name="value_spin" id="value_spin" cols="30" rows="10">@if(session('orig_word')){{ session('orig_word') }}@endif</textarea> 
            </div>
            <div class="field">
                <label for="">Pengecualian Kalimat </label>
                <div class="ui fluid multiple search selection dropdown">
                        <input type="hidden" name="exclude_spintext" id="exclude_spintext" value="{{ session('exclude') }}">
                        <div class="default text">Pisahkan kalimat dengan koma " , "</div>
                        <div class="menu">
                        </div>
                </div>
            </div>
            <div class='ui basic center aligned segment'>
                <div class="ui black button" id="btn_reset">Reset</div>
                <div class="ui green button" id="btn_spin">SpinText</div>
                <div class="ui blue button" id="btn_spin_f">Spin</div>
            </div>
            <br>
        @if (session('result'))
            <div class="field" id="result_areas">
                <label for="">Results SpinText :</label>
                <textarea name="result_spin" id="result_spin" cols="30" rows="10" >{{ session('result') }}</textarea>
            </div>
        @endif

        @if( session('final_articles') )
            <div class="field" id="final_articles">
                <label for="">Final Article Result : </label>
                <textarea name="final_result" id="final_result" cols="30" rows="10" readonly>{{ session('final_articles') }}</textarea>
            </div>
        @endif
        <div class="ui error message"></div>
    </form>
    <h4 class="ui horizontal divider header">
        <i class="info icon"></i>
        Description
    </h4>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis dolor aliquid fugit voluptatibus suscipit culpa neque fugiat consequatur deleniti tempora, porro tenetur. Cupiditate, rerum dignissimos aperiam sequi a eveniet maxime!
    </p>
    <div class="ui horizontal segments">
        <div class="ui segment teal inverted">
            <p>
            <h3 class='ui horizontal center aligned header'><i class="fighter jet icon"></i> Fast</h3>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt, voluptate saepe voluptatum magnam ut pariatur tempore tenetur dolore iste totam repudiandae sed, odit quae obcaecati, doloremque inventore labore quod eligendi.
            </p>
        </div>
        <div class="ui segment olive inverted">
            <p>
                <h3 class="ui center aligned header"><i class="wrench icon"></i> Customize your word</h3>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus facere itaque, delectus architecto deleniti eius velit repellendus vero doloribus ab veritatis illo eaque provident, amet quos quia! Ipsum, quos dolore?
            </p>
        </div>
        <div class="ui segment yellow inverted">
            <p>
                <h3 class="ui center aligned header"><i class="thumbs up icon"></i> Easy to use</h3>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate distinctio at illo, totam sapiente vero nisi maxime maiores sequi, sint nesciunt nulla ipsam assumenda quasi hic cupiditate laborum aspernatur dolorem!
            </p>
        </div>
    </div>
</div>
<div class="ui blue inverted vertical footer segment">
    <div class="ui container">
      <div class="ui stackable inverted divided equal height stackable grid">
        <div class="three wide column">
          <h4 class="ui inverted header">About</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Sitemap</a>
            <a href="#" class="item">Contact Us</a>
            <a href="#" class="item">Religious Ceremonies</a>
            <a href="#" class="item">Gazebo Plans</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">Services</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Banana Pre-Order</a>
            <a href="#" class="item">DNA FAQ</a>
            <a href="#" class="item">How To Access</a>
            <a href="#" class="item">Favorite X-Men</a>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Footer Header</h4>
          <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
        </div>
      </div>
    </div>
  </div>
<div class="ui grey inverted vertical center aligned segment">
    Copyright &copy 2019 Build with <i class="heart icon"></i> by Fatkhur R 
</div>
</body>
</html>