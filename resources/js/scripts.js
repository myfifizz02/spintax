jQuery( document ).ready(function($) {
    if($("#result_spin").length > 0){
        $("#btn_spin").attr("disabled",true).addClass('disabled')
        $("#btn_spin_f").removeAttr("disabled").removeClass('disabled')
        $("#primary_form").form({
            fields:{
                value_spin: {
                    identifier: 'value_spin',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Please enter your article above'
                      }
                    ]
                },
                
                result_spin:{
                    identifier: 'result_spin',
                    rules:  [
                        {
                            type    :   'empty',
                            prompt  :   'Result spin textarea can not be empty !'
                        }
                    ]
                },
                
            }
        })
    }else{
        $("#btn_spin_f").attr("disabled",true).addClass('disabled')
        $("#btn_spin").removeAttr("disabled").disabled
        $("#primary_form").form({
            fields:{
                value_spin: {
                    identifier: 'value_spin',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Please enter your article above'
                      }
                    ]
                },
            }
        })
    }
    
    $("#btn_spin").on("click",function(){
        $("#type_spin").val('spin')
        $(this).attr("disabled","true")
        $("#primary_form").trigger("submit")
    })
    $("#primary_form").on('submit',function(){
        let value_spin = $('#value_spin').val();
        let result_value = $("#result_spin").val();
        if( $("#result_spin").length > 0 ) {
            console.log(`result spin is ${result_value}`)
            if (value_spin != '' && result_value != '') {
                $("#primary_form").addClass('loading')
            }
        }else{
            if ( value_spin != '') {
                $("#primary_form").addClass('loading')
            }
        }
        
    })
    
    $("#btn_spin_f").on('click',function(){
        $("#type_spin").val('spin_final')
        $("#primary_form").trigger('submit')
    })


    $("#btn_reset").on('click',function(){
        $('form').form('reset');
        $('form .ui.error.message').empty();
        $("#value_spin").val("")
        $("#btn_spin").removeAttr('disabled')
        if($("#result_spin").length > 0){
            $("#btn_spin").removeClass('disabled')
            $("#result_areas").css('display','none')
            $("#btn_spin_f").attr("disabled",true).addClass('disabled')
        }

        if($("#final_result").length > 0 ){
            $("#final_articles").css('display','none')
        }
        if($("#exclude_spintext").val() != ''){
            $('.ui.dropdown').dropdown('clear')
        }
    })
    $(".ui.dropdown").dropdown({
        allowAdditions:true
    })
});
